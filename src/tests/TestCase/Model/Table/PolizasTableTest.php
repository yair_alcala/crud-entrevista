<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PolizasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PolizasTable Test Case
 */
class PolizasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PolizasTable
     */
    public $Polizas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Polizas',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Polizas') ? [] : ['className' => PolizasTable::class];
        $this->Polizas = TableRegistry::getTableLocator()->get('Polizas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Polizas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
