<h2>Crear producto</h2>
<div class="content">
  <?php
  echo $this->Form->create($inventario);
  echo $this->Form->control('newSku',['label'=>['text'=>'SKU']]);
  echo $this->Form->control('nombre');
  echo $this->Form->control('cantidad');
  echo $this->Form->submit('Guardar');
  echo $this->Form->end();
  ?>
</div>
<hr>
<?=
   $this->Html->link('Regresar al inicio',['action'=>'index']);
?>
