<h2>Vista de producto</h2>
<div class="content">
  <?php
  echo $this->Form->create($inventario);
  echo $this->Form->control('newSku',['disabled'=>true,'value'=>$inventario->sku,'label'=>['text'=>'SKU']]);
  echo $this->Form->control('nombre',['disabled'=>true]);
  echo $this->Form->control('cantidad',['disabled'=>true]);
  echo $this->Form->end();
  ?>
</div>
<hr>
<?=
   $this->Html->link('Regresar al inicio',['action'=>'index']);
?>
