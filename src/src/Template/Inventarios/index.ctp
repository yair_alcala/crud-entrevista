<h2>Inventario</h2>
<div class="content">
  <table>
    <thead>
      <tr>
        <th>SKU</th>
        <th>Nombre</th>
        <th>Cantidad</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($inventarios as $key => $inv): ?>
        <tr>
          <th><?= $inv->sku  ?></th>
          <th><?= $inv->nombre  ?></th>
          <th><?= $inv->cantidad  ?></th>
          <th>
            <?php
              echo $this->Form->postLink('Eliminar',['action' => 'delete', $inv->sku],['confirm'=>'¿Desea eliminar el producto '.$inv->nombre.'?']);
              echo " | ";
              echo $this->Html->link('Editar',['action' => 'edit', $inv->sku]);
              echo " | ";
              echo $this->Html->link('Ver',['action' => 'view', $inv->sku]);
            ?>
          </th>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <?php
    echo $this->Html->link('Crear producto',['action'=>'add']);
  ?>
</div>
