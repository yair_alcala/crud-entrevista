<div class="content">
  <table>
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Puesto</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($empleados as $key => $emp): ?>
        <tr>
          <th><?= $emp->nombre  ?></th>
          <th><?= $emp->apellido  ?></th>
          <th><?= $emp->puesto  ?></th>
          <th>
            <?php
              echo $this->Form->postLink('Eliminar',['action' => 'delete', $emp->idEmpleado],['confirm'=>'¿Desea eliminar el empleado '.$emp->nombre.'?']);
              echo " | ";
              echo $this->Html->link('Editar',['action' => 'edit', $emp->idEmpleado]);
              echo " | ";
              echo $this->Html->link('Ver',['action' => 'view', $emp->idEmpleado]);
            ?>
          </th>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <?php
    echo $this->Html->link('Crear empleado',['action'=>'add']);
  ?>
</div>
