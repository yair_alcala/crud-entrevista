<h2>Vista de empleado</h2>
<div class="content">
  <?php
  echo $this->Form->create($empleado);
  echo $this->Form->control('nombre',['disabled'=>true]);
  echo $this->Form->control('apellido',['disabled'=>true]);
  echo $this->Form->control('puesto',['disabled'=>true]);
  echo $this->Form->end();
  ?>
</div>
<hr>
<?=
   $this->Html->link('Regresar al inicio',['action'=>'index']);
?>
