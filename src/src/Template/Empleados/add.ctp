<h2>Crear empleado</h2>
<div class="content">
  <?php
  echo $this->Form->create($empleado);
  echo $this->Form->control('nombre');
  echo $this->Form->control('apellido');
  echo $this->Form->control('puesto');
  echo $this->Form->submit('Guardar');
  echo $this->Form->end();
  ?>
</div>
<hr>
<?=
   $this->Html->link('Regresar al inicio',['action'=>'index']);
?>
