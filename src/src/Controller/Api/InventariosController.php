<?php
namespace App\Controller\Api;

use App\Controller\AppController;

class InventariosController extends AppController
{
  public function index()
  {
    $this->autoRender = false;
    $inventarios = $this->Inventarios->find('all')
    ->where([
      'status'=>1,
      'deleted'=>0,
    ])->toArray();
    return $this->response->withType('application/json')->withStringBody(json_encode($inventarios));
  }

  public function view($id = null)
  {

  }

  public function add()
  {

  }

  public function edit($id = null)
  {

  }

  public function delete($id = null)
  {

  }
}
