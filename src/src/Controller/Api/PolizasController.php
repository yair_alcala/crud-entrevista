<?php
namespace App\Controller\Api;

use App\Controller\AppController;

class PolizasController extends AppController
{
  function validate($token = null)
  {
    if($token!=null){
      $ch = curl_init();
      $headers = array(
        "Authorization: ".$token,
        "Accept: application/json",
        "Content-Type: application/json"
      );
      $endpoint = "https://apigateway.coppel.com:58443/sso-dev/api/v1/verify";

      curl_setopt($ch, CURLOPT_URL, $endpoint);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $result = curl_exec($ch);
      $result = json_decode($result);
      curl_close($ch);
      return $result;
    }
  }

  function createLog($error){
    $log = fopen("files/logs/errors.log", "a");
    fwrite($log, $error."\n");
    fclose($log);
  }

  public function view($id = null)
  {
    $this->autoRender=false;
    $response_err = [
      "Meta"=>[
        "Status"=> 'FAILURE'
      ],
      "Data" => [
        "Mensaje" => "Ha ocurrido un error al consultar la póliza"
      ]
    ];
    $response = [
      "Meta"=>[
        "Status"=> "Ok"
      ],
      "Data" => []
    ];
    $result = $this->validate($this->request->getHeader('Authorization')[0]);
    if(mb_strtoupper($result->meta->status) != 'FAIL'){
      try {
        $poliza = $this->Polizas->find('all')
        ->select(['idPoliza','cantidad'])
        ->where(['idPoliza'=>$id])
        ->contain([
          'Empleados'=>[
            'fields'=>['nombre','Apellido']
          ],
          'Inventarios'=>[
            'fields'=>['sku','nombre']
          ],
          ])->toArray();
        if(!empty($poliza)){
          $data = [
            'Poliza' =>[
              'IDPoliza' => $poliza[0]->idPoliza,
              'Cantidad' => $poliza[0]->cantidad,
            ],
            "Empleado" => $poliza[0]->empleado,
            "DetalleArticulo" => $poliza[0]->inventario,
          ];
          $response['Data']=$data;
          return $this->response->withType('application/json')->withStringBody(json_encode($response));
        }else{
          $this->createLog(date('Y-m-d H:i:s').': Poliza no encontrada');
          return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
        }

      } catch (\Exception $e) {
        $this->createLog(date('Y-m-d H:i:s').': Regitro no encontrado');
        return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
      }
    }
    $this->createLog(date('Y-m-d H:i:s').': Roken no valido');
    return $this->response->withType('application/json')->withStringBody(json_encode($response_err));

  }

  public function add()
  {
    $this->autoRender=false;
    $response_err = [
      "Meta"=>[
        "Status"=> 'FAILURE'
      ],
      "Data" => [
        "Mensaje" => "Ha ocurrido un error al crear la póliza"
      ]
    ];
    $response = [
      "Meta"=>[
        "Status"=> "Ok"
      ],
      "Data" => []
    ];
    $result = $this->validate($this->request->getHeader('Authorization')[0]);
    if(mb_strtoupper($result->meta->status) != 'FAIL'){
      if($this->request->is('post')){
        $poliza = $this->Polizas->newEntity($this->request->getData());
        if($poliza->hasErrors()){
          $this->createLog(date('Y-m-d H:i:s').': Campos no validos o faltantes al guardar');
          return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
        }else{
          $isSaved = $this->Polizas->save($poliza);
          if($isSaved){
            $poliza = $this->Polizas->find('all')
            ->select(['idPoliza','cantidad'])
            ->where(['idPoliza'=>$isSaved->idPoliza])
            ->contain([
              'Empleados'=>[
                'fields'=>['nombre','Apellido']
              ],
              'Inventarios'=>[
                'fields'=>['sku','nombre','cantidad']
              ],
            ])->toArray();
            if($poliza[0]->cantidad<$poliza[0]->inventario->cantidad){
              $restarInventario = $this->Polizas->Inventarios->updateAll(
                ['cantidad'=>($poliza[0]->inventario->cantidad - $poliza[0]->cantidad)],
                ['sku'=>$poliza[0]->inventario->sku]
              );
              if($restarInventario){
                $data = [
                  'Poliza' =>[
                    'IDPoliza' => $poliza[0]->idPoliza,
                    'Cantidad' => $poliza[0]->cantidad,
                  ],
                  "Empleado" => $poliza[0]->empleado,
                  "DetalleArticulo" => $poliza[0]->inventario,
                ];

                $response['Data']=$data;
                return $this->response->withType('application/json')->withStringBody(json_encode($response));
              }
            }
          }else{
            $this->createLog(date('Y-m-d H:i:s').': Error al guardar');
            return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
          }
        }
      }
    }
    $this->createLog(date('Y-m-d H:i:s').': Error al agregar - Token no valido');
    return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
  }

  public function edit($id = null)
  {
    $this->autoRender=false;
    $response_err = [
      "Meta"=>[
        "Status"=> 'FAILURE'
      ],
      "Data" => [
        "Mensaje" => "Ha ocurrido un error al actualizar la póliza"
      ]
    ];
    $response = [
      "Meta"=>[
        "Status"=> "Ok"
      ],
      "Data" => []
    ];
    $result = $this->validate($this->request->getHeader('Authorization')[0]);
    if(mb_strtoupper($result->meta->status) != 'FAIL'){
      if($this->request->is(['post','put'])){
        try {
          $poliza = $this->Polizas->get($id);
          $polizaEdit = $this->Polizas->patchEntity($poliza,$this->request->getData());
          if(!$polizaEdit->hasErrors()){
            $isSaved = $this->Polizas->save($polizaEdit);
            if($isSaved){
              if(!empty($this->request->getData('cantidad'))){
                $cantidad = $this->Polizas->Inventarios->find('all')
                ->select(['cantidad'])
                ->where(['sku'=>$isSaved->sku])->toArray()[0]->cantidad;
                if($this->request->getData('cantidad') < $cantidad){
                  $restarInventario = $this->Polizas->Inventarios->updateAll(
                    ['cantidad'=>($cantidad - $this->request->getData('cantidad'))],
                    ['sku'=>$isSaved->sku]
                  );
                  if($restarInventario){
                    $response['Data']['Mensaje']='Se actualizó correctamente la Poliza '.$isSaved->idPoliza;
                    return $this->response->withType('application/json')->withStringBody(json_encode($response));
                  }
                }
              }

              $response['Data']['Mensaje']='Se actualizó correctamente la Poliza '.$isSaved->idPoliza;
              return $this->response->withType('application/json')->withStringBody(json_encode($response));
            }else{
              $this->createLog(date('Y-m-d H:i:s').': Error al guardar edición');
              return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
            }
          }else{
            $this->createLog(date('Y-m-d H:i:s').': Campos faltantes o erroneos al editar');
            return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
          }
        } catch (\Exception $e) {
          $this->createLog(date('Y-m-d H:i:s').': Error al buscar el registro');
          return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
        }
      }
    }
    $this->createLog(date('Y-m-d H:i:s').': Error al editar - Token no valido');
    return $this->response->withType('application/json')->withStringBody(json_encode($response_err));

  }

  public function delete($id = null)
  {
    $response_err = [
      "Meta"=>[
        "Status"=> 'FAILURE'
      ],
      "Data" => [
        "Mensaje" => "Ha ocurrido un error al eliminar la póliza"
      ]
    ];
    $response = [
      "Meta"=>[
        "Status"=> "Ok"
      ],
      "Data" => [
        "Mensaje" => "Se eliminó correctamente la poliza ".$id
      ]
    ];
    if($this->request->is('delete')){
      $result = $this->validate($this->request->getHeader('Authorization')[0]);
      if(mb_strtoupper($result->meta->status) != 'FAIL'){
        try{
          $isUpdated = $this->Polizas->deleteAll(
            ['idPoliza'=>$id],
          );
          if($isUpdated){
            return $this->response->withType('application/json')->withStringBody(json_encode($response));
          }else{
            $this->createLog(date('Y-m-d H:i:s').': Error al eliminar - el registro no existe');
            return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
          }
        }catch (\Exception $e){
          $this->createLog(date('Y-m-d H:i:s').': Error al eliminar registro');
          return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
        }
      }
      $this->createLog(date('Y-m-d H:i:s').': Error al eliminar - Token no valido');
      return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
    }
    $this->createLog(date('Y-m-d H:i:s').': Error al eliminar - método incorrecto');
    return $this->response->withType('application/json')->withStringBody(json_encode($response_err));
  }
}
