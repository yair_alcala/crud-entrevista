<?php
namespace App\Controller;

use App\Controller\AppController;

class EmpleadosController extends AppController
{
  public function index()
  {
    $empleados = $this->Empleados->find('all')
    ->where([
      'status'=>1,
      'deleted'=>0,
    ])->toArray();

    $this->set(compact('empleados'));

  }

  public function view($id = null)
  {
    $empleado = $this->Empleados->get($id);
    $this->set(compact('empleado'));
  }

  public function add()
  {
    $empleado = $this->Empleados->newEntity();
    if ($this->request->is('post') && !empty($this->request->getData())) {
      $newEmp = $this->Empleados->patchEntity($empleado, $this->request->getData());
      if(!$newEmp->hasErrors()){
        $isSaved = $this->Empleados->save($newEmp);
        $this->redirect(['action'=>'index']);
      }
    }
    $this->set(compact('empleado'));

  }

  public function edit($id = null)
  {
    $empleado = $this->Empleados->get($id);
    if($this->request->is(['post','put'])){
      $newEmp = $this->Empleados->patchEntity($empleado, $this->request->getData());
      if(!$newEmp->hasErrors()){
        $isSaved = $this->Empleados->save($newEmp);
        $this->redirect(['action'=>'index']);
      }
    }
    $this->set(compact('empleado'));
  }

  public function delete($id = null)
  {
    $this->autoRender= false;
    if($this->request->is('post')){
      $isSaved = $this->Empleados->updateAll(
        ['deleted'=>1],
        ['idEmpleado'=>$id]
      );
      if($isSaved){
        $this->redirect(['action'=>'index']);
      }
    }

  }
}
