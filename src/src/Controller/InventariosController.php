<?php
namespace App\Controller;

use App\Controller\AppController;

class InventariosController extends AppController
{
  public function initialize(){
    parent::initialize();
    $this->loadComponent('RequestHandler');
  }

  public function index()
  {
    $inventarios = $this->Inventarios->find('all')
    ->where([
      'status'=>1,
      'deleted'=>0,
    ])->toArray();

    $this->set(compact('inventarios'));
  }

  public function view($id = null)
  {
    $inventario = $this->Inventarios->get($id);
    $this->set(compact('inventario'));
  }

  public function add()
  {
    $inventario = $this->Inventarios->newEntity();
    if ($this->request->is('post') && !empty($this->request->getData())) {
      $data = [
        'sku' => $this->request->getData('newSku'),
        'nombre' => $this->request->getData('nombre'),
        'cantidad' => $this->request->getData('cantidad')
      ];
      $newInv = $this->Inventarios->patchEntity($inventario, $data);
      if(!$newInv->hasErrors()){
        $isSaved = $this->Inventarios->save($newInv);
        $this->redirect(['action'=>'index']);
      }
    }
    $this->set(compact('inventario'));

  }

  public function edit($id = null)
  {
    $inventario = $this->Inventarios->get($id);
    if($this->request->is(['post','put'])){
      $newInv = $this->Inventarios->patchEntity($inventario, $this->request->getData());
      if(!$newInv->hasErrors()){
        $isSaved = $this->Inventarios->save($newInv);
        $this->redirect(['action'=>'index']);
      }
    }
    $this->set(compact('inventario'));
  }

  public function delete($id = null)
  {
    $this->autoRender= false;
    if($this->request->is('post')){
      $isSaved = $this->Inventarios->updateAll(
        ['deleted'=>1],
        ['sku'=>$id]
      );
      if($isSaved){
        $this->redirect(['action'=>'index']);
      }
    }

  }
}
