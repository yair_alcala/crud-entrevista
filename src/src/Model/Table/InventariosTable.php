<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
* Inventarios Model
*
* @method \App\Model\Entity\Inventario get($primaryKey, $options = [])
* @method \App\Model\Entity\Inventario newEntity($data = null, array $options = [])
* @method \App\Model\Entity\Inventario[] newEntities(array $data, array $options = [])
* @method \App\Model\Entity\Inventario|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
* @method \App\Model\Entity\Inventario saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
* @method \App\Model\Entity\Inventario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
* @method \App\Model\Entity\Inventario[] patchEntities($entities, array $data, array $options = [])
* @method \App\Model\Entity\Inventario findOrCreate($search, callable $callback = null, $options = [])
*
* @mixin \Cake\ORM\Behavior\TimestampBehavior
*/
class InventariosTable extends Table
{
  /**
  * Initialize method
  *
  * @param array $config The configuration for the Table.
  * @return void
  */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('inventarios');
    $this->setDisplayField('sku');
    $this->setPrimaryKey('sku');

    $this->addBehavior('Timestamp');

    $this->hasMany('Polizas', [
      'propertyName' => 'sku_poliza',
      'foreignKey' => 'sku',
    ]);

  }

  public function buildRules(RulesChecker $rules){

    $rules->add($rules->isUnique(['sku'],'sku en uso'));

    return $rules;
  }

  /**
  * Default validation rules.
  *
  * @param \Cake\Validation\Validator $validator Validator instance.
  * @return \Cake\Validation\Validator
  */
  public function validationDefault(Validator $validator)
  {
    $validator
    ->scalar('sku')
    ->maxLength('sku', 25)
    ->allowEmptyString('sku', null, 'create');

    $validator
    ->scalar('nombre')
    ->maxLength('nombre', 255)
    ->requirePresence('nombre', 'create')
    ->notEmptyString('nombre');

    $validator
    ->integer('cantidad')
    ->requirePresence('cantidad', 'create')
    ->notEmptyString('cantidad');

    $validator
    ->notEmptyString('status');

    $validator
    ->notEmptyString('deleted');

    return $validator;
  }


}
