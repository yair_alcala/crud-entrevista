<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
* Empleados Model
*
* @method \App\Model\Entity\Empleado get($primaryKey, $options = [])
* @method \App\Model\Entity\Empleado newEntity($data = null, array $options = [])
* @method \App\Model\Entity\Empleado[] newEntities(array $data, array $options = [])
* @method \App\Model\Entity\Empleado|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
* @method \App\Model\Entity\Empleado saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
* @method \App\Model\Entity\Empleado patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
* @method \App\Model\Entity\Empleado[] patchEntities($entities, array $data, array $options = [])
* @method \App\Model\Entity\Empleado findOrCreate($search, callable $callback = null, $options = [])
*
* @mixin \Cake\ORM\Behavior\TimestampBehavior
*/
class EmpleadosTable extends Table
{
  /**
  * Initialize method
  *
  * @param array $config The configuration for the Table.
  * @return void
  */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('empleados');
    $this->setDisplayField('idEmpleado');
    $this->setPrimaryKey('idEmpleado');

    $this->addBehavior('Timestamp');

    $this->hasMany('Polizas', [
      'foreignKey' => 'empleadoGenero',
    ]);

  }

  public function buildRules(RulesChecker $rules){

    $rules->add($rules->isUnique(['idEmpleado'],'id de empleado en uso'));

    return $rules;
  }

  /**
  * Default validation rules.
  *
  * @param \Cake\Validation\Validator $validator Validator instance.
  * @return \Cake\Validation\Validator
  */
  public function validationDefault(Validator $validator)
  {
    $validator
    ->integer('idEmpleado')
    ->allowEmptyString('idEmpleado', null, 'create');

    $validator
    ->scalar('nombre')
    ->maxLength('nombre', 100)
    ->requirePresence('nombre', 'create')
    ->notEmptyString('nombre');

    $validator
    ->scalar('apellido')
    ->maxLength('apellido', 100)
    ->requirePresence('apellido', 'create')
    ->notEmptyString('apellido');

    $validator
    ->scalar('puesto')
    ->maxLength('puesto', 50)
    ->allowEmptyString('puesto');

    $validator
    ->notEmptyString('status');

    $validator
    ->notEmptyString('deleted');

    return $validator;
  }

}
