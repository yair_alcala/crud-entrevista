<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
* Polizas Model
*
* @method \App\Model\Entity\Poliza get($primaryKey, $options = [])
* @method \App\Model\Entity\Poliza newEntity($data = null, array $options = [])
* @method \App\Model\Entity\Poliza[] newEntities(array $data, array $options = [])
* @method \App\Model\Entity\Poliza|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
* @method \App\Model\Entity\Poliza saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
* @method \App\Model\Entity\Poliza patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
* @method \App\Model\Entity\Poliza[] patchEntities($entities, array $data, array $options = [])
* @method \App\Model\Entity\Poliza findOrCreate($search, callable $callback = null, $options = [])
*
* @mixin \Cake\ORM\Behavior\TimestampBehavior
*/
class PolizasTable extends Table
{
  /**
  * Initialize method
  *
  * @param array $config The configuration for the Table.
  * @return void
  */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->setTable('polizas');
    $this->setDisplayField('idPoliza');
    $this->setPrimaryKey('idPoliza');

    $this->addBehavior('Timestamp');

    $this->belongsTo('Empleados', [
      'foreignKey' => 'empleadoGenero',
      'joinType' => 'INNER',
    ]);

    $this->belongsTo('Inventarios', [
      'foreignKey' => 'sku',
      'joinType' => 'INNER',
    ]);

  }

  public function buildRules(RulesChecker $rules){

    $rules->add($rules->isUnique(['idPoliza'],'id de poliza en uso'));

    return $rules;
  }

  /**
  * Default validation rules.
  *
  * @param \Cake\Validation\Validator $validator Validator instance.
  * @return \Cake\Validation\Validator
  */
  public function validationDefault(Validator $validator)
  {
    $validator
    ->integer('idPoliza')
    ->allowEmptyString('idPoliza', null, 'create');

    $validator
    ->integer('empleadoGenero')
    ->requirePresence('empleadoGenero', 'create')
    ->notEmptyString('empleadoGenero');

    $validator
    ->scalar('sku')
    ->maxLength('sku', 25)
    ->requirePresence('sku', 'create')
    ->notEmptyString('sku');

    $validator
    ->integer('cantidad')
    ->requirePresence('cantidad', 'create')
    ->notEmptyString('cantidad');

    $validator
    ->date('fecha')
    ->allowEmptyDate('fecha');

    return $validator;
  }

}
