<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Inventario Entity
 *
 * @property string $sku
 * @property string $nombre
 * @property int $cantidad
 * @property int $status
 * @property int $deleted
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Inventario extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sku' => true,
        'nombre' => true,
        'cantidad' => true,
        'status' => true,
        'deleted' => true,
        'created' => true,
        'modified' => true,
    ];
}
