<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Poliza Entity
 *
 * @property int $idPoliza
 * @property int $empleadoGenero
 * @property string $sku
 * @property int $cantidad
 * @property \Cake\I18n\FrozenDate|null $fecha
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Poliza extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'empleadoGenero' => true,
        'sku' => true,
        'cantidad' => true,
        'fecha' => true,
        'created' => true,
        'modified' => true,
    ];
}
